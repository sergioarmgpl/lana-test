import unittest
from Basket import *

class TestBasket(unittest.TestCase):
    def setUp(self):
        self.basket = Basket()

    def test_backets_cases1(self):
        #case 1
        # 32.50 PEN TSHIRT MUG
        data = self.basket.new()
        id = data["bid"]
        self.basket.addProduct({"bid":id,"code":"PEN"})
        self.basket.addProduct({"bid":id,"code":"TSHIRT"})
        self.basket.addProduct({"bid":id,"code":"MUG"})
        self.assertEqual(32.50, self.basket.getTotal({"bid":id})["total"])
        data = {"bid":id}
        self.basket.remove(data)

    def test_backets_cases2(self):
        #case 2
        # 25 PEN TSHIRT PEN
        data = self.basket.new()
        id = data["bid"]
        self.basket.addProduct({"bid":id,"code":"PEN"})
        self.basket.addProduct({"bid":id,"code":"TSHIRT"})
        self.basket.addProduct({"bid":id,"code":"PEN"})
        self.assertEqual(25.0, self.basket.getTotal({"bid":id})["total"])
        data = {"bid":id}
        self.basket.remove(data)

    def test_backets_cases3(self):
        #case 3
        # 65 TSHIRT, TSHIRT, TSHIRT, PEN, TSHIRT
        data = self.basket.new()
        id = data["bid"]
        self.basket.addProduct({"bid":id,"code":"TSHIRT"})
        self.basket.addProduct({"bid":id,"code":"TSHIRT"})
        self.basket.addProduct({"bid":id,"code":"TSHIRT"})
        self.basket.addProduct({"bid":id,"code":"PEN"})
        self.basket.addProduct({"bid":id,"code":"TSHIRT"})
        self.assertEqual(65.0, self.basket.getTotal({"bid":id})["total"])
        data = {"bid":id}
        self.basket.remove(data)

    def test_backets_cases4(self):
        #case 4
        # 62.5 PEN, TSHIRT, PEN, PEN, MUG, TSHIRT, TSHIRT
        data = self.basket.new()
        id = data["bid"]
        self.basket.addProduct({"bid":id,"code":"PEN"})
        self.basket.addProduct({"bid":id,"code":"TSHIRT"})
        self.basket.addProduct({"bid":id,"code":"PEN"})
        self.basket.addProduct({"bid":id,"code":"PEN"})
        self.basket.addProduct({"bid":id,"code":"MUG"})
        self.basket.addProduct({"bid":id,"code":"TSHIRT"})
        self.basket.addProduct({"bid":id,"code":"TSHIRT"})
        self.assertEqual(62.5, self.basket.getTotal({"bid":id})["total"])
        data = {"bid":id}
        self.basket.remove(data)

if __name__ == '__main__':
    unittest.main()
