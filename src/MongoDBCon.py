from pymongo import MongoClient
from bson.json_util import dumps, loads
import hashlib
import json
import os
import sys

class MongoDBCon:
   url = os.environ['MONGOSTRING']
   client = None
   db = None
   collection = None

   #Init Connection
   def __init__(self,collection):
      self.client = MongoClient(self.url,retrywrites=False,maxPoolSize=None)
      self.db = self.client.lana
      self.collection = self.db[collection]

   #Drop collection
   def drop(self):
      self.collection.drop()

   #Find multiple documents
   def find(self,query={},fields={}):
      if not len(fields):
         data = self.collection.find(query)         
      else:
         data=self.collection.find(query,fields)
      return data

   #Find one document
   def findOne(self,query={}):
      return self.collection.find_one(query)

   #Insert a document 
   def insert(self,data={}):
      return self.collection.insert_one(data)
   
   #Update a document
   def update(self,filter,data={}):
      data2 = { "$set": data }
      return self.collection.update_one(filter,data2)  

   #Push an element into an array
   def push(self,filter,data={}):
      data2 = { "$push": data }
      return self.collection.update_one(filter,data2)   

   #Delete a document
   def delete(self,filter):
      return self.collection.delete_one(filter)