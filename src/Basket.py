from MongoDBCon import *
from bson.objectid import ObjectId
import math


class Basket:
    mdb = None
    #Create a new checkout basket
    def __init__(self):
        self.mdb = MongoDBCon('baskets')
    def new(self):
      data = {
          "products":list()
      }
      r = self.mdb.insert(data)
      data = {}
      data["bid"] = str(r.inserted_id)
      return data

    def getAll(self):
        data = {}
        data["baskets"] = []
        cursor = self.mdb.find()
        for document in cursor:
            document["_id"]=str(document["_id"])
            data["baskets"].append(document)
        return data

    #Add a product to a basket
    def addProduct(self,data):
        filter = {"_id":ObjectId(data["bid"])}
        data2 = {}
        data2 = {"products":data["code"]}
        self.mdb.push(filter,data2)
        return data

    #Get the total amount in a basket
    def getTotal(self,data):
        filter = {"_id":ObjectId(data["bid"])}
        basket = self.mdb.findOne(filter)
        products = basket["products"]
        npen = ntshirt = nmug = 0
        data2 = {} 
        data2["items"] = []
        for prod in products:
            data2["items"].append(prod)
            if prod == "PEN":
                npen +=1
            elif prod == "TSHIRT":
                ntshirt +=1
            elif prod == "MUG":
                nmug += 1
        #PEN offer two of the same product, get one free
        pen_price = 5.0
        total_pen = 0
        #total to pay for the pens
        total_pen = math.ceil(npen/2) * pen_price

        tshirt_price = 20.0
        total_tshirt = 0
        if ntshirt >= 3:
            total_tshirt = ntshirt * tshirt_price * 0.75
        else:
            total_tshirt = ntshirt * tshirt_price

        mug_price = 7.5
        total_mug = nmug * mug_price

        data2["total"] = total_pen + total_tshirt + total_mug

        return data2

    #Remove the basket
    def remove(self,data):
        #pending..
        filter = {"_id":ObjectId(data["bid"])}
        self.mdb.delete(filter)
        return data

