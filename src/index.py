from flask import Flask, request
from flask import jsonify
from Basket import *

app = Flask(__name__)

b = Basket()

# Just a health check
@app.route("/")
def url_root():
    return "OK"

# Just a health check
@app.route("/_health")
def url_health():
    return "OK2"

# Just a health check
@app.route("/ping")
def ping():
    return "pong"


#new
@app.route("/basket/", methods=["PUT"])
def newBasket():
    res = b.new()
    return jsonify(res)

#add
@app.route("/basket/<bid>/product/<code>", methods=["PUT"])
def addProduct(bid,code):
    data = {"bid":bid,"code":code}
    res = b.addProduct(data)
    return jsonify(res)

@app.route("/baskets/", methods=["GET"])
def getAll():
    res = b.getAll() 
    return jsonify(res)

@app.route("/basket/<bid>", methods=["DELETE"])
def deleteBasket(bid):
    data = {"bid":bid}
    res = b.remove(data)
    return jsonify(res)

@app.route("/basket/<bid>/total", methods=["GET"])
def getTotal(bid):
    data = {"bid":bid}
    res = b.getTotal(data)
    return jsonify(res)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)