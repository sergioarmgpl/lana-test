# Links

- https://docs.gitlab.com/ee/user/packages/container_registry/
- https://docs.gitlab.com/ee/ci/variables/README.html
- https://www.youtube.com/channel/UCKEXLx57HLkAKhXd5Q1VsbQ/videos
- https://docs.gitlab.com/ee/ci/pipelines/pipeline_architectures.html#basic-pipelines


# Install Docker
```
sudo apt-get update
sudo apt-cache madison docker.io
docker.io | 19.03.6-0ubuntu1~18.04.3
sudo apt-get install -y docker.io=19.03.6-0ubuntu1~18.04.3
sudo usermod -aG docker developer
relogin
```

# Install Runner

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
export GITLAB_RUNNER_DISABLE_SKEL=true; sudo -E apt-get install gitlab-runner

(OPCIONAL)
apt-cache madison gitlab-runner
(OPCIONAL)
export GITLAB_RUNNER_DISABLE_SKEL=true; sudo -E apt-get install gitlab-runner=10.0.0

systemctl status gitlab-runner


Runner Installation
https://docs.gitlab.com/runner/install/linux-repository.html


https://docs.gitlab.com/ee/ci/docker/using_docker_build.html

https://docs.gitlab.com/runner/register/index.html
Registrar Runner
Obtain a token:
- For a project-specific runner, go to Settings > CI/CD and expand the Runners section
Enable shared runners for this project -> OFF
Check for the registration token: <token>
sudo gitlab-runner register


sudo gitlab-runner register --docker-privileged --docker-volumes "/certs/client"

https://gitlab.com/
<token>
docker-runner
docker
tmaier/docker-compose


# Runner configuration:
/etc/gitlab-runner/config.toml

- When registering a runner on GitLab.com, the gitlab-ci coordinator URL is https://gitlab.com.





