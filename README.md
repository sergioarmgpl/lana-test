# Lana Test
This is the manual for the technical challenge at Lana
  
## Available Commands
Here are the available API to manage baskets:
  
Create Basket: Creates a new basket and returns the basket id
```
curl -X PUT http://domain.tld:5001/basket/
```
Output
```
{
  "bid": "601cff703da5f52957cc585d"
}
```
   

Get Baskets: Returns all the baskets
```
curl -X GET http://domain.tld:5001/baskets/
```
Output
```
{
  "baskets": [
    {
      "_id": "601cff703da5f52957cc585d",
      "products": [
        "PEN"
      ]
    }
  ]
}
```
   
   
Add Product to Basket: Add a producto to a specific basket
```
curl -X PUT http://domain.tld:5001/basket/<BASKET_ID>/product/<PRODUCT_CODE>
```
Output
```
{
  "bid": "601cff703da5f52957cc585d",
  "code": "PEN"
}
```
   
   
Get the total from a specific basket
```
curl -X GET http://domain.tld:5001/basket/<BASKET_ID>/total
```
```
{
  "items": [
    "PEN"
  ],
  "total": 5.0
}
```
   

Delete Basket: Delete a specific basket using its id
```
curl -X DELETE http://domain.tld:5001/basket/<BASKET_ID>
```
Output
```
{
  "bid": "601cff703da5f52957cc585d"
}
```
   
Right now the API is deployed using the domaing lanatest.tk
   
   
## Enviroments
This API is deployed in 2 environments:
- Development, Port 5001
- Production, Port 5002
   
To access dev API you have to access the domain.tld in the port 5001 of the host and for the prod API you have to access the domain.tld in the port 5002
   
## Access monitoring
To Access the monitoring application(cAdvisor) you have to enter the next URL:
- http://domain.tld:8080/  
cAdvisor was installed using the next link as a reference:  
- https://github.com/google/cadvisor
   
## Examples
All the examples contains the next steps:
- Create a basket
- Add a product
- Get the total
- Delete the basket
   
Case 1: PEN TSHIRT MUG  
Returns: 32.50  
   
Commands:  
```
curl -X PUT http://domain.tld:5001/basket/
curl -X PUT http://domain.tld:5001/basket/<BASKET_ID>/product/PEN
curl -X PUT http://domain.tld:5001/basket/<BASKET_ID>/product/TSHIRT
curl -X PUT http://domain.tld:5001/basket/<BASKET_ID>/product/MUG
curl -X GET http://domain.tld:5001/basket/<BASKET_ID>/total
curl -X DELETE http://domain.tld:5001/basket/<BASKET_ID>
```
   
   
Case 2: PEN TSHIRT PEN  
Return: 25.0  
   
Commands:  
```
curl -X PUT http://domain.tld:5001/basket/
curl -X PUT http://domain.tld:5001/basket/<BASKET_ID>/product/PEN
curl -X PUT http://domain.tld:5001/basket/<BASKET_ID>/product/TSHIRT
curl -X PUT http://domain.tld:5001/basket/<BASKET_ID>/product/PEN
curl -X GET http://domain.tld:5001/basket/<BASKET_ID>/total
curl -X DELETE http://domain.tld:5001/basket/<BASKET_ID>
```
   
Case 3: TSHIRT TSHIRT TSHIRT PEN TSHIRT  
Return: 65.0  
   
Commands:  
```
curl -X PUT http://domain.tld:5001/basket/
curl -X PUT http://domain.tld:5001/basket/<BASKET_ID>/product/TSHIRT
curl -X PUT http://domain.tld:5001/basket/<BASKET_ID>/product/TSHIRT
curl -X PUT http://domain.tld:5001/basket/<BASKET_ID>/product/TSHIRT
curl -X PUT http://domain.tld:5001/basket/<BASKET_ID>/product/PEN
curl -X PUT http://domain.tld:5001/basket/<BASKET_ID>/product/TSHIRT
curl -X GET http://domain.tld:5001/basket/<BASKET_ID>/total
curl -X DELETE http://domain.tld:5001/basket/<BASKET_ID>
```
   
Case 4: PEN, TSHIRT, PEN, PEN, MUG, TSHIRT, TSHIRT  
Return: 62.5  
   
Commands: 
   
```
curl -X PUT http://domain.tld:5001/basket/
curl -X PUT http://domain.tld:5001/basket/<BASKET_ID>/product/PEN
curl -X PUT http://domain.tld:5001/basket/<BASKET_ID>/product/TSHIRT
curl -X PUT http://domain.tld:5001/basket/<BASKET_ID>/product/PEN
curl -X PUT http://domain.tld:5001/basket/<BASKET_ID>/product/PEN
curl -X PUT http://domain.tld:5001/basket/<BASKET_ID>/product/MUG
curl -X PUT http://domain.tld:5001/basket/<BASKET_ID>/product/TSHIRT
curl -X PUT http://domain.tld:5001/basket/<BASKET_ID>/product/TSHIRT
curl -X GET http://domain.tld:5001/basket/<BASKET_ID>/total
curl -X DELETE http://domain.tld:5001/basket/<BASKET_ID>
```
   
## MongoDB
We selected MongoDB as a primary Database, because of the benefits of unstructured data in order to be ready for Machine Learning or Analytics on realtime, which is a common use case for NoSQL Databases

## Repository & Files
This repository is organized in the next folders and files:
-  src: This folder contains the source code of the API
- Basket.py: Contains all the logic of the API, all the data is stored in MongoDB
- Dockerfile: File configuration to build a the API as a Docker image, this image is stored at GitLab
- index.py: This file contains the API interfaces that deliver the Basket services as an API using the Basket.py logic
- MongoDBCon.py: This contains a basic library to connect to MongoDB
- requirements.txt: This file contains all the required python libraries to build the application
- run.sh: This file could be used for local testing and development
- tests.py: This files executes some Unit Tests to the API, implements the 4 examples of the original repository
   
   
## Pipeline design

### Stages
Because is a simple instance, we are changing between ports to deploy the container, we can set a different DNS at the moment to have some differences accesing the API. We already have the next environments
- Dev: Deployed automatically
- Prod: Deployed manually if Dev API runs as expected

### Container naming
For naming convention we use:  
gitlab-repo:COMMIT_SHORT_SHA-CI_PIPELINE_ID  
   
Explanation:
- gitlab-repo
- COMMIT_SHORT_SHA
- CI_PIPELINE_ID
   
Example:
- registry.gitlab.com/sergioarmgpl/lana-test:9189809e-251874228
   
This creates a unique image name useful to perform rollbacks
   
   
## GitLab configuration & CI/CD Workflow
### Pipelines
This CI/CD Pipeline is using gitlab private repositories and the next variables:
- ID_RSA: Includes de SSH Key to deploy the new container from the pipeline
- MONGOSTRING:  Includes de DB Connection to MongoDB
- SERVER_HOST: Host where the container is deployed
- SERVER_USER: The username to Login into the SERVER_HOST using SSH
   
In the same instance is configured a Gitlab Docker Runner
   
### CI/CD Workflow
To execute the pipeline you have to do the next steps:
1. Modify your code
2. Commit and Push to the master branch or merge a branch into the master
3. GitLab detects the change and execute the test-build, to do some unit tests, and build the image, the next step will be deploy-dev that uses the previous image to deploy it into the SERVER_HOST, the api is available at the 5001 port, and the if everything runs nice, you have to execute manually the step to deploy to production that deploys the image into the SERVER_HOST and its available using the port 5002
4. Access the API using curl

## Video
This is a video explaining this CI/CD Pipeline:
- https://youtu.be/k6Bm68spnyk

## Current Domains used
- API: http://api.dev.lanatest.tk:5001/, http://api.prod.lanatest.tk:5002/
- Monitoring: http://lanatest.tk:8080/

## Future Improvements
- Use different servers to deploy dev and prod services
- Migrate to Kubernetes if the system grow fast in order to improve pipelines, deployments and scalability
- Use a gateway to manage the traffic to this APIs, for example Traefik
- Use JWT or an Authentication Tokens to protect this API
- Get prices from the Database to dont modify the code